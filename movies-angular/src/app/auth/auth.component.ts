import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AuthService } from "./auth.service";
import { Router } from "@angular/router";
import { ReCaptchaV3Service } from 'ng-recaptcha';

 @Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrl: './auth.component.css'
 })

 export class AuthComponent {
    isLoginMode = false;
    isLoading = false;
    error: string = null;
    signUpSuccessfully = false;
    reCAPTCHAToken: string = null;

    constructor(private authService: AuthService, private router: Router, private recaptchaV3Service: ReCaptchaV3Service) {}

    onSwitchMode() {
        this.isLoginMode = !this.isLoginMode;
    }

    onSubmit(form: NgForm) {
        this.error = null;
        this.isLoading = true;
        let email = form.value.email;
        let password = form.value.password;
        this.signUpSuccessfully = false;

        this.recaptchaV3Service.execute('importantAction').subscribe((token) =>{
            this.reCAPTCHAToken = `${token}`;   

            if(this.isLoginMode) {
                this.authService.signIn(email, password, this.reCAPTCHAToken).subscribe({
                    next: (response) => {
                        this.isLoading = false;
                        this.router.navigate(['/movies'])
                    },
                    error: (error) => {
                        this.isLoading = false;
                        this.error = error;
                    }
                })

            } else {
                this.authService.signUp(email, password, this.reCAPTCHAToken).subscribe({
                    next: (response) => {
                        this.isLoading = false
                        this.signUpSuccessfully = true;

                    },
                    error: (error) => {
                        this.error = error;
                        this.isLoading = false
                    }
                });
            }
        });
        
      form.reset()
        
    }
 }