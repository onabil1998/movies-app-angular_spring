import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, tap } from "rxjs/operators";
import { BehaviorSubject, Subject, throwError } from "rxjs";
import { User } from "./user.model";
import { Router } from "@angular/router";
import { environment } from "../../environments/environment.development";

class AuthResponseData {
    jwtToken: string;
    email: string;
    expiresIn: string;
}

@Injectable()
export class AuthService {

    user = new BehaviorSubject<User>(null);
    private tokenExpirationTimer: any;
    
    constructor(private http: HttpClient, private router: Router) {}

    signUp(email: string, password: string, reCAPTCHAToken: string) {

        const headers = new HttpHeaders({
            'ReCaptcha': `${reCAPTCHAToken}`,
            'Content-Type': 'application/json'
        });

        return this.http.post<AuthResponseData>(
            `${environment.REGISTER_URL}`,
            {
                email: email,
                password: password,
            },
            { headers: headers }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    signIn(email: string, password: string, reCAPTCHAToken: string) {
        
        const headers = new HttpHeaders({
            'ReCaptcha': `${reCAPTCHAToken}`,
            'Content-Type': 'application/json'
        });
        
        return this.http.post<AuthResponseData>(
            `${environment.AUTHENTICATE_URL}`,
            {
                email: email,
                password: password,
            },
            { headers: headers }
        )
        .pipe(
            catchError(this.handleErrors),
            tap(resData => {
                this.handleAuthentication(
                    resData.email, 
                    resData.jwtToken, 
                    +resData.expiresIn
                    )
                })
            );
    }

    logOut() {
        this.user.next(null);
        this.router.navigate(['/auth']);
        localStorage.removeItem('userData');
        if(this.tokenExpirationTimer) {
            clearTimeout(this.tokenExpirationTimer);
        }
        this.tokenExpirationTimer = null;
    }

    autoLogIn() {
        const userData : {
            email: string,
            _token: string,
            _tokenExpirationDate: string
        } = JSON.parse(localStorage.getItem('userData'));

        if(!userData) {
            return;
        }
        const loadedUser = new User(userData.email, userData._token, new Date(userData._tokenExpirationDate));

        if(loadedUser.token) {
            const expirationDuration = new Date(userData._tokenExpirationDate).getTime() - new Date().getTime();
            this.user.next(loadedUser);
            this.autoLogOut(expirationDuration);
        }
    }

    autoLogOut(expirationDuration: number) {
        this.tokenExpirationTimer = setTimeout(() => {
            this.logOut();
        }, expirationDuration)
    }

    private handleAuthentication(email: string, token: string, expiresIn: number) {
        const expirationDate = new Date(new Date().getTime() + expiresIn * 1000);
        const user = new User(
            email,
            token,
            expirationDate
        );
        this.user.next(user);
        this.autoLogOut(expiresIn * 1000);
        localStorage.setItem('userData', JSON.stringify(user));
    }

    handleErrors(errorRes: HttpErrorResponse) {
        console.log(errorRes);
        let errorMessage = "Unknown error occured";
        if(!errorRes.message) {
            return throwError(() => errorMessage);
        }
        switch (errorRes.message) {
            case 'EMAIL_EXISTS' :
                errorMessage = "This Email is already registered";
                break;
            case 'EMAIL_NOT_FOUND' :
                errorMessage = "Invalid Credentials";
                break;
            case 'INVALID_PASSWORD' :
                errorMessage = "Invalid Credentials";
                break;
            default :
                errorMessage = errorRes.message;
        }
        return throwError(() => errorMessage);
    }
}