import { AuthComponent } from "./auth.component";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { AuthGuard } from "./auth.guard";
import { NgModule } from "@angular/core";
import { sharedModule } from "../shared/shared.module";
import { BrowserModule } from "@angular/platform-browser";
import { RECAPTCHA_V3_SITE_KEY, RecaptchaV3Module } from "ng-recaptcha";
import { environment } from "../../environments/environment.development";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";

@NgModule({
    declarations: [
      AuthComponent
    ],
    imports: [
      FormsModule,
      sharedModule,
      BrowserModule,
      RecaptchaV3Module,
      MatProgressSpinnerModule,
      RouterModule.forChild([{path: "auth", component: AuthComponent, canActivate: [AuthGuard]}])
    ],
    providers: [
      {
        provide: RECAPTCHA_V3_SITE_KEY,
        useValue: environment.recaptcha.siteKey,
      }]
  })
  export class AuthModule { }
  