import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment.development";

@Injectable()
export class MoviesService {

    constructor(private http: HttpClient) {}

    getMovieDetails(movie_id: string): Observable<any>{

        const userToken: string = this.getUserToken();
        if (!userToken) {
            return;
        }

        const headers = new HttpHeaders({
            'Authorization': `Bearer ${userToken}`,
            'Content-Type': 'application/json'
        });
            
        return this.http.get(`${environment.GET_MOVIE_DETAILS_URL}/${movie_id}`,{ headers: headers })
    }

    getTopRatedMoviesPage(pageNumber: string): Observable<any> {

        const userToken: string = this.getUserToken();
        if (!userToken) {
            return;
        }

        const headers = new HttpHeaders({
            'Authorization': `Bearer ${userToken}`,
            'Content-Type': 'application/json',
        });

        return this.http.get(`${environment.GET_ALL_MOVIES_URL}?page=${pageNumber}`,{ headers: headers });
    }

    getMovieCast(movie_id: string): Observable<any> {
        return this.http.get(`https://api.themoviedb.org/3/movie/${movie_id}/credits?api_key=${environment.tmdbAPIKey}`)
      }

    getUserToken(): string | null {
        const userData: {
          email: string,
          _token: string,
          _tokenExpirationDate: string
        } = JSON.parse(localStorage.getItem('userData'));
      
        return userData ? userData._token : null;
    }
}