import { ComponentFixture, TestBed } from '@angular/core/testing';
import { environment } from '../../../environments/environment.development';

import { MoviesCatalogComponent } from './movies-catalog.component';
import { MoviesService } from '../services/movies.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { of } from 'rxjs';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';

describe('MoviesCatalogComponent', () => {
  let component: MoviesCatalogComponent;
  let fixture: ComponentFixture<MoviesCatalogComponent>;
  let moviesService: MoviesService;
  let httpTestController: HttpTestingController;
  let el: DebugElement;
  beforeEach(async () => {
    
    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [MoviesCatalogComponent],
      providers: [
        MoviesService
      ],
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MoviesCatalogComponent);
    component = fixture.componentInstance;
    moviesService = TestBed.inject(MoviesService);
    httpTestController = TestBed.inject(HttpTestingController);
    fixture.detectChanges();
    el = fixture.debugElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call the getTopRatedMoviesPage function', () => {
    const mockResult = { results: [{ title: 'Movie 1' }, { title: 'Movie 2' }] };
    spyOn(moviesService, 'getTopRatedMoviesPage').and.returnValue(of(mockResult));
    component.ngOnInit();
    expect(component.topRatedResults).toEqual([{ title: 'Movie 1' }, { title: 'Movie 2' }]);
  });

  it('should call the http endpoint for the movies list', () => {
    const mockReq = httpTestController.expectOne(`https://api.themoviedb.org/3/movie/top_rated?page=1&api_key=${environment.tmdbAPIKey}`)
    expect(mockReq.request.method).toEqual('GET');
    mockReq.flush({ results: [{ title: 'Movie 1' }, { title: 'Movie 2' }] });
    expect(component.topRatedResults).toEqual([{ title: 'Movie 1' }, { title: 'Movie 2' }]);
  });

  it('should render the movie list', () => {
    component.topRatedResults = [
      { id: 1, title: 'Movie 1', vote_average: 8.5, poster_path: 'blabla' }
    ];
    component.topRatedResults[0]['title'] = "testing";
    fixture.detectChanges();
    const movieRateElement = el.queryAll(By.css('.card-title'));
    expect(movieRateElement[0].nativeElement.textContent).toBe("testing")
  });

});
