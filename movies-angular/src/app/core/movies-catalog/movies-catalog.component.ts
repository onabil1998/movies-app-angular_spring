import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../services/movies.service';

@Component({
  selector: 'app-movies-catalog',
  templateUrl: './movies-catalog.component.html',
  styleUrl: './movies-catalog.component.css'
})
export class MoviesCatalogComponent implements OnInit {

  topRatedResults = [];
  pageNumber = 0;
  constructor(private moviesService: MoviesService) {}

  ngOnInit(): void {
    this.getMoviesPage();
  }

  getMoviesPage() {
    this.moviesService.getTopRatedMoviesPage(this.pageNumber.toString()).subscribe((result) => {
      this.topRatedResults.push(...result);
    })
    this.pageNumber++;
  }
}
