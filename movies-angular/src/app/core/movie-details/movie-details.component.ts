import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from '../services/movies.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrl: './movie-details.component.css'
})
export class MovieDetailsComponent implements OnInit {

  movieDetailsArray: any;
  movieCastArray: any;
  id: string;

  constructor(private route: ActivatedRoute, private moviesService: MoviesService) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.movieDetails(this.id);
    this.movieCast(this.id);
  }

  movieDetails(movie_id: string) {
    this.moviesService.getMovieDetails(movie_id).subscribe((result) => {
      this.movieDetailsArray = result;
    })
  }

  movieCast(movie_id: string) {
    this.moviesService.getMovieCast(movie_id).subscribe((result) => {
      this.movieCastArray = result.cast;
      this.movieCastArray.length=20;
    })
  }

}
