export const environment = {
    GET_ALL_MOVIES_URL: 'http://localhost:8081/api/movies',
    GET_MOVIE_DETAILS_URL: 'http://localhost:8081/api/movies',
    tmdbAPIKey: '09b9f30c6585d5439a6b5b0064eea184',
    AUTHENTICATE_URL: 'http://localhost:8080/api/v1/auth/authenticate',
    REGISTER_URL: 'http://localhost:8080/api/v1/auth/register',
    recaptcha: {
        siteKey: '6LcB0GMpAAAAAGn9fiqSgn2tvzYNai_QBi4EUSWJ',
    },
};
