package com.moviesApp.apigateway.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReCaptchaResponse {
    Boolean success;
    String challege_ts;
    String hostname;
    Double score;
    String action;
}
