package com.moviesApp.apigateway.config;

import com.moviesApp.apigateway.dto.ReCaptchaResponse;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;

@RequiredArgsConstructor
@Component
public class ReCaptchaFilter extends OncePerRequestFilter {

    @Value("${recaptcha.verifyUrl}")
    private String verifyUrl;

    @Value("${recaptcha.secretKey}")
    private String secretKey;

    private final WebClient webClient;

    @Override
    protected void doFilterInternal(
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain
    ) throws ServletException, IOException {
        final String reCaptchaHeader = request.getHeader("ReCaptcha");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String,String> map = new LinkedMultiValueMap<>();
        map.add("secret", secretKey);
        map.add("response",reCaptchaHeader);

        ReCaptchaResponse reCaptchaResponse = webClient.post()
                .uri(verifyUrl)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .headers(httpHeaders -> httpHeaders.addAll(headers))
                .body(BodyInserters.fromFormData(map))
                .retrieve()
                .bodyToMono(ReCaptchaResponse.class)
                .block();

        if (reCaptchaResponse.getScore() > 0.5) {
            filterChain.doFilter(request, response);
            return;
        }
        response.setStatus(403);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        String path = request.getRequestURI();
        return path.startsWith("/api/v1/auth/validate");
    }
}
