package com.moviesApp.apigateway.entity;

public enum Role {

    USER,
    ADMIN
}
