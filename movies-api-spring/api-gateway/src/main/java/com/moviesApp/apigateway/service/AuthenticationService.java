package com.moviesApp.apigateway.service;

import com.moviesApp.apigateway.config.JwtService;
import com.moviesApp.apigateway.dto.AuthenticationRequest;
import com.moviesApp.apigateway.dto.AuthenticationResponse;
import com.moviesApp.apigateway.dto.RegisterRequest;
import com.moviesApp.apigateway.entity.Role;
import com.moviesApp.apigateway.entity.User;
import com.moviesApp.apigateway.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public AuthenticationResponse authenticate(AuthenticationRequest authenticationRequest) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getEmail(),
                        authenticationRequest.getPassword()
                )
        );

        User user = userRepository.findByEmail(authenticationRequest.getEmail()).orElseThrow(() -> new UsernameNotFoundException("user not found"));
        String jwtToken = jwtService.generateToken(user);
        String expiresIn = String.valueOf((jwtService.extractExpiration(jwtToken).getTime() - System.currentTimeMillis()) / 1000);
        return AuthenticationResponse.builder()
                .jwtToken(jwtToken)
                .email(user.getEmail())
                .expiresIn(expiresIn)
                .build();
    }

    public AuthenticationResponse register(RegisterRequest registerRequest) {
        User user = User.builder()
                .email(registerRequest.getEmail())
                .password(passwordEncoder.encode(registerRequest.getPassword()))
                .role(Role.USER)
                .build();

        userRepository.save(user);
        String jwtToken = jwtService.generateToken(user);
        String expiresIn = String.valueOf((jwtService.extractExpiration(jwtToken).getTime() - System.currentTimeMillis()) / 1000);
        return AuthenticationResponse.builder()
                .jwtToken(jwtToken)
                .email(user.getEmail())
                .expiresIn(expiresIn)
                .build();
    }
}
