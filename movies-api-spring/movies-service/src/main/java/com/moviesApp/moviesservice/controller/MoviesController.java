package com.moviesApp.moviesservice.controller;

import com.moviesApp.moviesservice.model.Movie;
import com.moviesApp.moviesservice.repository.MovieRepository;
import com.moviesApp.moviesservice.service.MoviesService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
@CrossOrigin()
public class MoviesController {

    private final MoviesService moviesService;

    @GetMapping("/movies")
    public List<Movie> getAllMovies(@RequestParam(defaultValue = "0",name = "page")  int page) {
        return moviesService.getAllMovies(page);
    }

    @GetMapping("/movies/{id}")
    public Movie getOneMovie(@PathVariable long id) {
        return moviesService.getOneMovie(id);
    }

}
