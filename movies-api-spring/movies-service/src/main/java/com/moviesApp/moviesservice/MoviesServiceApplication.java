package com.moviesApp.moviesservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moviesApp.moviesservice.model.Movie;
import com.moviesApp.moviesservice.repository.MovieRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class MoviesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoviesServiceApplication.class, args);
	}

	@Bean
	public CommandLineRunner loadData(MovieRepository movieRepository, ObjectMapper objectMapper) {
		return args -> {
			String filePath = "D:\\Final project\\movies-api-spring\\movies-service\\src\\main\\java\\com\\moviesApp\\moviesservice\\util\\movies.json";

			Movie[] moviesArray = objectMapper.readValue(new File(filePath), Movie[].class);

			List<Movie> movies = Arrays.asList(moviesArray);
			movieRepository.saveAll(movies);
		};
	}

}
