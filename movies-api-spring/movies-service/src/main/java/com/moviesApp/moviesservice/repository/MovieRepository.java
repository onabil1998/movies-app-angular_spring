package com.moviesApp.moviesservice.repository;

import com.moviesApp.moviesservice.model.Movie;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MovieRepository extends MongoRepository<Movie, Long> {
}
