package com.moviesApp.moviesservice.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "movies")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Movie {

    @Id
    private Long id;
    private String title;
    private String overview;
    private String releaseDate;
    private boolean adult;
    private String backdropPath;
    private List<Integer> genreIds;
    private String originalLanguage;
    private String originalTitle;
    private double popularity;
    private String posterPath;
    private boolean video;
    private double voteAverage;
    private int voteCount;
}