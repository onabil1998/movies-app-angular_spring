package com.moviesApp.moviesservice.service;

import com.moviesApp.moviesservice.model.Movie;
import com.moviesApp.moviesservice.repository.MovieRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MoviesService {

    private final MovieRepository movieRepository;

    public List<Movie> getAllMovies(int page) {

        Pageable paging = PageRequest.of(page, 20);
        Page<Movie> moviesPage = movieRepository.findAll(paging);

        return moviesPage.getContent();
    }

    public Movie getOneMovie(long id) {
        Optional<Movie> movieOptional = movieRepository.findById(id);
        return movieOptional.orElseThrow(() -> new RuntimeException("Movie Not found"));
    }
}
