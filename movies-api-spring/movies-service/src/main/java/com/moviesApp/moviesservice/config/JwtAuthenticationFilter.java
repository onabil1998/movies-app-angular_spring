package com.moviesApp.moviesservice.config;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final WebClient webClient;
    private String AUTH_URL = "http://127.0.0.1:8080/api/v1/auth/validate";

    @Override
    protected void doFilterInternal(
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain
    ) throws ServletException, IOException {
        final String authHeader = request.getHeader("Authorization");
        final String jwtToken;

        if(authHeader == null || !authHeader.startsWith("Bearer ")) {
            response.setStatus(403);
            return;
        }
        jwtToken = authHeader.substring(7);
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(jwtToken);
        try {
            ClientResponse clientResponse = webClient.get()
                    .uri(AUTH_URL)
                    .headers(httpHeaders -> httpHeaders.addAll(headers))
                    .exchange()
                    .block();
            if (clientResponse != null && clientResponse.statusCode().is2xxSuccessful()) {
                filterChain.doFilter(request, response);
            }
            else {
                response.setStatus(403);
            }
        } catch (Exception e) {
            response.setStatus(401);
        }
    }
}
