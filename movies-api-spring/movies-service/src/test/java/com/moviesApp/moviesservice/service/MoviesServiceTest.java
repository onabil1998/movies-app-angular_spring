package com.moviesApp.moviesservice.service;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import com.moviesApp.moviesservice.model.Movie;
import com.moviesApp.moviesservice.repository.MovieRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.springframework.http.HttpStatus.OK;

@AutoConfigureMockMvc
@SpringBootTest
@WireMockTest(httpPort = 8080)
class MoviesServiceTest {

    private static WireMockServer wireMockServer;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MovieRepository movieRepository;

    private Movie movie;

    @BeforeEach
    void init() {
        movie = Movie.builder()
                .id(1L)
                .title("Testing Title")
                .adult(false)
                .video(false)
                .originalLanguage("Test")
                .voteAverage(8)
                .backdropPath("Testing")
                .backdropPath("Testing")
                .popularity(10)
                .posterPath("Testing")
                .voteCount(5)
                .overview("Testing Overview")
                .releaseDate("Testing Date")
                .originalTitle("Testing")
                .build();
    }

    @Test
    void shouldThrowExceptionWhenMovieNotFound() throws Exception {
        long id = 1;
        stubFor(WireMock.get(urlMatching("/api/v1/auth/validate"))
                .willReturn(aResponse()
                        .withStatus(OK.value())));

        Mockito.when(movieRepository.findById(id)).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/movies/{id}",id)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer MockToken"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());

    }

    @Test
    void shouldGetOneMovie() throws Exception {
        long id = 1;

        stubFor(WireMock.get(urlMatching("/api/v1/auth/validate"))
                .willReturn(aResponse()
                        .withStatus(OK.value())));

        Mockito.when(movieRepository.findById(id)).thenReturn(Optional.of(movie));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/movies/{id}",id)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer MockToken"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(movie.getTitle()));

    }
}